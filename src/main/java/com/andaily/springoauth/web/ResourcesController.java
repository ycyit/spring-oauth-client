package com.andaily.springoauth.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.andaily.springoauth.service.OauthService;
import com.andaily.springoauth.service.dto.UserDto;

/**
 * Handle visit Oauth resources, must be have 'access_token'
 *
 * @author Shengzhao Li
 */
@Controller
public class ResourcesController {

	@Value("#{properties['unityUserInfoUri']}")
	private String			unityUserInfoUri;

	@Autowired
	private OauthService	oauthService;

	/*
	 * Visit unity role for get user information from oauth server
	 */
	@RequestMapping("unity_user_info")
	public String unityUserInfo(String access_token, Model model) {
		UserDto userDto = oauthService.loadUnityUserDto(access_token, unityUserInfoUri);

		if (userDto.error()) {
			// error
			model.addAttribute("message", userDto.getErrorDescription());
			model.addAttribute("error", userDto.getError());
			return "redirect:oauth_error";
		} else {
			model.addAttribute("userDto", userDto);
			return "resources/unity_user_info";
		}

	}

}
