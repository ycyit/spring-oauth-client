package com.andaily.springoauth.web;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.andaily.springoauth.service.OauthService;
import com.andaily.springoauth.service.dto.UserDto;
import com.andaily.springoauth.service.password.PasswordParams;

/**
 * 密码端模式业务处理控制层
 *
 */
@Controller
public class PasswordController {

	private static final Logger	LOG	= LoggerFactory.getLogger(PasswordController.class);
	@Value("#{properties['access-token-uri']}")
	private String				accessTokenUri;
	@Autowired
	private OauthService		oauthService;

	/*
	 * Entrance: step-1
	 */
	@RequestMapping(value = "password", method = RequestMethod.GET)
	public String password(Model model) {
		LOG.debug("Go to 'password' page, accessTokenUri = {}", accessTokenUri);
		model.addAttribute("accessTokenUri", accessTokenUri);
		return "password";
	}

	@RequestMapping(value = "password", method = RequestMethod.POST)
	public void getAccessToken() {
		PasswordParams params = new PasswordParams(accessTokenUri).setClientId("mobile-client")
				.setClientSecret("mobile").setUsername("mobile").setPassword("mobile");
		Map<String, Object> resultMap = oauthService.loadMobileUserDto(params);
		UserDto userDto = (UserDto) resultMap.get("userDto");
		if ((boolean) resultMap.get("flag")) {
			if (userDto.error()) {
				System.out.println(userDto.getErrorDescription());
			} else {
				System.out.println(userDto.getUsername());
			}
		} else {
			System.out.println(userDto.getErrorDescription());
		}
	}

}
