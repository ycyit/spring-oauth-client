package com.andaily.springoauth.service.dto;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 请求服务端code所需数据
 */
public class AuthorizationCodeDto implements Serializable {

	private static final long	serialVersionUID	= -5347432772215338779L;
	private String				userAuthorizationUri;
	private String				responseType;
	private String				scope;
	private String				clientId;
	private String				clientSecret;
	private String				redirectUri;
	private String				state;

	public AuthorizationCodeDto() {
	}

	public String getUserAuthorizationUri() {
		return userAuthorizationUri;
	}

	public void setUserAuthorizationUri(String userAuthorizationUri) {
		this.userAuthorizationUri = userAuthorizationUri;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getRedirectUri() {
		return redirectUri;
	}

	public void setRedirectUri(String redirectUri) {
		this.redirectUri = redirectUri;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	public String getFullUri() throws UnsupportedEncodingException {
		String redirect = URLEncoder.encode(redirectUri, "UTF-8");
		return String.format("%s?response_type=%s&scope=%s&client_id=%s&redirect_uri=%s&state=%s",
				userAuthorizationUri, responseType, scope, clientId, redirect, state);
	}
}
