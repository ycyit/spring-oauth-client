package com.andaily.springoauth.service.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户信息
 */
public class UserDto extends AbstractOauthDto {

	private static final long	serialVersionUID	= 7491457618506413376L;
	private boolean				archived;
	private String				email;
	private String				guid;

	private String				phone;
	private String				username;

	private List<String>		privileges			= new ArrayList<>();

	public UserDto() {
	}

	public UserDto(String error, String errorDescription) {
		this.error = error;
		this.errorDescription = errorDescription;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<String> privileges) {
		this.privileges = privileges;
	}
}
