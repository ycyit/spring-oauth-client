package com.andaily.springoauth.service;

import java.util.Map;

import com.andaily.springoauth.service.dto.AccessTokenDto;
import com.andaily.springoauth.service.dto.AuthAccessTokenDto;
import com.andaily.springoauth.service.dto.AuthCallbackDto;
import com.andaily.springoauth.service.dto.AuthorizationCodeDto;
import com.andaily.springoauth.service.dto.RefreshAccessTokenDto;
import com.andaily.springoauth.service.dto.UserDto;
import com.andaily.springoauth.service.password.PasswordParams;

public interface OauthService {

	String submitAuthorizationCode(AuthorizationCodeDto codeDto);

	Map<String, Object> authorizationCodeCallback(AuthCallbackDto callbackDto);

	AccessTokenDto retrieveAccessTokenDto(AuthAccessTokenDto tokenDto);

	AuthAccessTokenDto createAuthAccessTokenDto(AuthCallbackDto callbackDto);

	AccessTokenDto retrievePasswordAccessTokenDto(AuthAccessTokenDto authAccessTokenDto);

	AccessTokenDto refreshAccessTokenDto(RefreshAccessTokenDto refreshAccessTokenDto);

	Map<String, Object> retrieveCredentialsAccessTokenDto(AuthAccessTokenDto authAccessTokenDto);

	Map<String, Object> loadMobileUserDto(PasswordParams params);

	UserDto loadUnityUserDto(String accessToken, String infoUri);
}
