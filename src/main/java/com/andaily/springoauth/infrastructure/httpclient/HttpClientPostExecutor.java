package com.andaily.springoauth.infrastructure.httpclient;

import org.apache.http.client.methods.RequestBuilder;

/**
 * http请求模式：post
 */
public class HttpClientPostExecutor extends HttpClientExecutor {

	public HttpClientPostExecutor(String url) {
		super(url);
	}

	protected RequestBuilder createRequestBuilder() {
		return RequestBuilder.post();
	}
}
